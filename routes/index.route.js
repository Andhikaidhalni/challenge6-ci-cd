const express = require('express')
const router = express.Router()
// const authRoutes = require('./auth.route')
const user_gamesroute = require('./user_games.route')
const AuthController = require('../controller/auth.controller')
const { body, validationResult } = require('express-validator');

const jwt = require ('jsonwebtoken')

router.get('/usergames',
(req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
    }
  }
  }, AuthController.list)

  router.get('/usergames/:id',
  (req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
      
    }
  }
  },
  AuthController.getById)

  router.put('/usergames/:id',
  (req, res, next) => {
  if (req.headers.authorization) {
    const user = jwt.decode(req.headers.authorization)
    req.user = user
    next()
  } else {
    throw {
      status: 401,
      message: 'Unauthorized request'
    }
  }
},
  [
  body('password')
  .optional()
  .notEmpty()
  ],
  (req, res, next) => {
  const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: "Usename or Password failed"
      }
    } else {
      next()
    }
  },
  AuthController.update)

  router.delete('/usergames/:id', (req, res, next) => {
    if (req.headers.authorization) {
      const user = jwt.decode(req.headers.authorization)
      req.user = user
      next()
    } else {
      throw {
        status: 401,
        message: 'Unauthorized request'
      }
    }
  }, AuthController.delete)






// router.post('/', AuthController.login)
router.post('/register', AuthController.register)
router.post('/loginpassword', AuthController.loginPassword)
router.post('/login-google', AuthController.loginGoogle)

// router.use('/login', authRoutes)
// router.use('/register', authRoutes)
// router.use('/loginpassword', authRoutes)
router.use('/usergame', user_gamesroute)



module.exports = router