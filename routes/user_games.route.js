const express = require('express')
const router = express.Router()
const usergamesController = require('../controller/user_gamesController')
// const auth = require('../middleware/auth')
const jwt = require ('jsonwebtoken')
const { body, validationResult } = require('express-validator');

const multer = require('multer')
const storage = require('../services/multerStorage.service')
const upload = multer({
    storage,
    limits: {
      fileSize: 3000000000000000000
    },
    fileFilter: (req, file, cb) => {
      if (file.mimetype === 'video/mp4') {
        cb(null, true)
      } else {
        cb({
          status: 400,
          message: 'File type does not match'
        }, false)
      }
    }
  })

  router.post('/',
  upload.single('video'),
  [
  body('username').notEmpty(),
  body('password').notEmpty(),
],
  (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      throw {
        status: 400,
        message: "Data cannot be empty" 
      }
    } else {
      next()
    }
  },usergamesController.create
)

// router.get('/1',
// (req, res, next) => {
//   if (req.headers.authorization) {
//     const user = jwt.decode(req.headers.authorization)
//     req.user = user
//     next()
//   } else {
//     throw {
//       status: 401,
//       message: 'Unauthorized request'
//     }
//   }
//   }, usergamesController.list)







// router.post('/', auth, usergamesController.create)
// router.get('/', auth, usergamesController.listUser)
// router.get('/:id', auth, usergamesController.getById)
// router.put('/:id', auth, usergamesController.update)
// router.delete('/:id', auth, usergamesController.delete)


module.exports = router