const express = require('express')
const app = express()
const port = 3000
const router = require('./routes/index.route.js');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');


app.use(express.urlencoded({extended: false}))
app.use(express.json())
app.use(router)

// app.use('/doc', swaggerUI.serve, swaggerUI.setup(swaggerJSON))
app.use('/api-docs', swaggerUi.serve);
app.get('/api-docs', swaggerUi.setup(swaggerDocument));

//list CRUD
//app.get('/usergames', user_gamesController.listUser)
// app.get('/usergamesbiodatas', GamesController.listBiodatas)
// app.get('/usergameshistories', GamesController.listHistories)
// app.post('/usergames', GamesController.create)
// app.put('/usergames/:id', GamesController.update)
// app.delete('/usergames/:id', GamesController.delete)

app.listen(port, () => {
    console.log(`Berjalan di port ${port}`)
})

module.exports = app