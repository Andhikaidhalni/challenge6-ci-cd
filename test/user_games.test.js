const request = require('supertest')
const { sequelize } = require('../models/index')
const { queryInterface } = sequelize
const bcrypt = require('bcryptjs')
const app = require('../app')
const jwt = require('jsonwebtoken')

let token;

beforeEach(async () => {
  // memasukkan data dummy ke database testing
  const salt = bcrypt.genSaltSync(10)
  const hash = bcrypt.hashSync("alif233", salt)
  await queryInterface.bulkInsert('user_games', [
    {
      username: "AlifSetiawan",
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])
  token = jwt.sign({
    id: 1,
    username: 'AlifSetiawan'
  }, 'usedse')
})

afterEach(async () => {
    await sequelize.query(
        "TRUNCATE TABLE ONLY user_games RESTART IDENTITY CASCADE"
      );
    
//  await queryInterface.bulkDelete('user_games', {}, { truncate: true, restartIdentity: true })
})

describe('POST user_games', () => {
  it('success', (done) => {
    request(app)
    .post('/usergame')
    .set("authorization", token)
    .send({
      "username": "Ilham Wahyudi",
      "password": "ilham1234"
  })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(201)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Succesfully create')
        done()
      }
    })
  })
  it('No auth', (done) => {
    request(app)
    .post('/usergame')
    .send({
      "username": "Ilham Wahyudi",
      "password": "ilham1234"
  })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Invalid auth token', (done) => {
    request(app)
    .post('/usergame')
    .set("authorization", "qweqwe")
    .send({
      "username": "Ilham Wahyudi",
      "password": "ilham1234"
  })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
})
describe('GET user_games', () => {
  it('success', (done) => {
    request(app)
    .get('/usergame')
    .set('authorization', token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(Array.isArray(res.body)).toBe(true)
        done()
      }
    })
  })
  it('no auth', (done) => {
    request(app)
    .get('/usergame')
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('invalid token', (done) => {
    request(app)
    .get('/usergame')
    .set('authorization', 'qweqwe')
    .end((err, res) => {
      if(err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
})
describe('GET user_games by ID', () => {
  it('Success', (done) => {
    request(app)
    .get('/usergame/1')
    .set('authorization', token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('username')
        expect(res.body).toHaveProperty('password')
        done()
      }
    })
  })
  it('No auth', (done) => {
    request(app)
    .get('/usergame/1')
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
  it('Invalid token', (done) => {
    request(app)
    .get('/usergame/1')
    .set('authorization', 'qweqwe')
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })  
})
describe('UPDATE /user_games/:id', () => {
  it('Success', (done) => {
    request(app)
    .put('/usergame/29')
    .set('authorization', token)
    .send({
      "username": "Ilham Wahyudi Ningsih",
      "password": "ilham12345"
  })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('successfully update user_games')
        done()
      }
    })
  })

  it('No Auth', (done) => {
    request(app)
    .put('/usergame/1')
    .send({
      "username": "Ilham Wahyudii Ningsih",
      "password": "ilham123445"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })

  it('Invalid Token', (done) => {
    request(app)
    .put('/usergame/1')
    .set('authorization', 'qweqwe')
    .send({
      "username": "Ilham Wahyudii Ningsih",
      "password": "ilham123445"
    })
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
})
describe('DELETE /user_games/:id', () => {

  it('Success', (done) => {
    request(app)
    .delete('/usergame/1')
    .set('authorization', token)
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(200)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Successfully delete data')
        done()
      }
    })
  })

  it('No auth', (done) => {
    request(app)
    .delete('/usergame/1')
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })

  it('Invalid token', (done) => {
    request(app)
    .delete('/usergame/1')
    .set('authorization', 'qweqwe')
    .end((err, res) => {
      if (err) {
        done(err)
      } else {
        expect(res.status).toBe(401)
        expect(res.body).toHaveProperty('message')
        expect(res.body.message).toBe('Unauthorized request')
        done()
      }
    })
  })
})