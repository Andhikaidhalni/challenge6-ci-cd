const { user_games } = require('../models')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const {OAuth2Client} = require('google-auth-library')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID, process.env.GOOGLE_CLIENT_SECRET)

class AuthController {
  //token lama
  // static async login(req, res, next) {
  //   try {
  //     const user = await user_games.findOne({
  //       where: {
  //         username: req.body.username
  //       }
  //     })

  //     if (!user) {
  //       throw {
  //         status: 401,
  //         message: 'Invalid username or password'
  //       }
  //     }
  //     let passreq = bcrypt.hashSync(req.body.password, salt)
  //     console.log(passreq,user.password)
  //     if (bcrypt.compareSync(req.body.password, user.password)) {
  //       const token = jwt.sign({
  //         id: user.id,
  //         username: user.username
  //       }, 'usedse')

  //       res.status(200).json({
  //         token
  //       })
  //     } else {
  //       console.log(user)
  //       throw {
  //         status: 401,
  //         message: 'Invalid username or password'
  //       }
  //     }
  //   } catch (err) {
  //     next(err)
  //   }
  // }
  static async register(req, res, next) {
    try {
      const salt = bcrypt.genSaltSync(10)
      const hash = bcrypt.hashSync(req.body.password, salt)

      const user = await user_games.findOne({
        where: {
          username: req.body.username
        }
      })
      await user_games.create({
        username: req.body.username,
        email: req.body.email,
        password: hash
      })

      res.status(200).json({
        message: 'Successfully create user'
      })
    } catch (err) {
      next(err)
    }
  }

  static async loginPassword(req, res, next) {
    try {
      const user = await user_games.findOne({
        where: {
          username: req.body.username
        }
      })

      if (!user) {
        throw {
          status: 401,
          message: 'Invalid username or password'
        }
      } else {
        if (bcrypt.compareSync(req.body.password, user.password)) {
          const token = jwt.sign({
            id: user.id,
            username: user.username
          }, process.env.JWT_SECRET)

          res.status(200).json({
            token,
          })
        } else {
          throw {
            status: 401,
            message: 'Invalid username or password'
          }
        }
      }
    } catch(err) {
      next(err)
    }
  }
  static async loginGoogle(req, res, next) {
    try {
      const token = await client.verifyIdToken({
        idToken: req.body.id_token,
        audience: process.env.GOOGLE_CLIENT_IDs
      })
      const payload = token.getPayload()

      const user = await user_games.findOne({
        where: {
          email: payload.email
        }
      })
      if (user) {
        const jwtToken = jwt.sign({
          id: user.id,
          email: user.email
        }, process.env.JWT_SECRET)
        res.status(200).json({
          token: jwtToken
        })
      } else {
        const registeredUser = await user_games.create({
          email: payload.email
        })
        const jwtToken = jwt.sign({
          id: registeredUser.id,
          email: registeredUser.email
        }, process.env.JWT_SECRET)
        res.status(200).json({
          token: jwtToken
        })
      }
    } catch(err) {
      next(err)
    }
  }
 
  static async list(req, res) {
    user_games.findAll({
      attributes: ['id', 'username','video'],
    })
      .then((data) => {
        res.status(200).json(data)
      })
      .catch((error) => {
        res.status(500).json({error})
      })
  }
  static async getById(req, res, next) {
    try {
      console.log(req.user)
      const user = await user_games.findOne({
         where: {
          id: req.user.id,
        },
      })
      if (!user) {
        throw {
          status: 404,
          message: 'User Not Found'
        }
      } else {
        res.status(200).json(user)
      }
    } catch (err) {
      next(err)
    }
  }

  static async update(req, res,next) {
    try {
     const user = await user_games.findOne({
        where: {
         id: req.user.id,
       },
     })

     if (!user) {
       throw {
         status: 404,
         message: 'User Not Found'
       }
     } else {
       await user_games.update(req.body, {
         where: {
           id: req.params.id
         }
       })
       res.status(200).json({
         message: 'Successfully Update User'
       })
     }
   } catch (err) {
     next(err)
   }   
 }

 static async delete(req, res, next) {
  try {
    const user = await user_games.findOne({
       where: {
        id: req.user.id,
      },
    })
    if (!user) {
      throw {
        status: 404,
        message: 'User Not Found'
      }
    } else {
    user_games.destroy({
    where: {
      id: req.params.id
    }
    })
      res.status(200).json({
        message: 'Successfully Delete User'
      })
    }
  } catch (err) {
    next(err)
 }
}


}

module.exports = AuthController