'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_games_histories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      user_games_histories.belongsTo(models.user_games, { foreignKey: 'user_game_id', as: 'Id User Game' })
      // define association here
    }
  }
  user_games_histories.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true
    },
    win: DataTypes.INTEGER,
    lose: DataTypes.INTEGER,
    time_list: DataTypes.TIME,
    user_game_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_games_histories',
  });
  return user_games_histories;
};