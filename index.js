const app = require("./app.js");
const http = require("http");
const port = 3000
// const port = process.env.PORT;
require('dotenv').config()
const server = http.createServer(app);

server.listen(port, async () => {
  console.log(`Berjalan di port 3000`);
});